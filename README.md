# ubuntu-rust-mini

A Docker image based on Ubuntu including a minimal Rust toolchain.

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the docker image: `make build`

## Update

* Create a new branch
```shell
git checkout -b nightly-2020-05-15
```
* Update the `RUST_VERSION` variable in `./Dockerfile` & `./Makefile`
```shell
sed -e 's!\(RUST_VERSION\)=.*$!\1=nightly-2020-05-15!' -i Dockerfile
sed -e 's!\(RUST_VERSION?\)=.*$!\1=nightly-2020-05-15!' -i Makefile
```
* Commit your changes
```shell
git add ./Dockerfile ./Makefile
git commit -m 'update(toolchain): switch to `nightly-2020-05-15`'
```
* Push the new branch
```shell
git push origin nightly-2020-05-15
```
* Wait for [CI/CD Pipelines](https://gitlab.com/docking/ubuntu-rust-mini/pipelines) to finish & check result
* Create a new tag, matching `${CUSTOM_VERSION}-${UBUNTU_VERSION}-${RUST_VERSION}`
```shell
git tag 0.4-18.04-nightly-2020-05-15
```
* Push the new tag
```shell
git push origin 0.4-18.04-nightly-2020-05-15
```
* Wait for [CI/CD Pipelines](https://gitlab.com/docking/ubuntu-rust-mini/pipelines) to finish & check result
* Check [Container Registry](https://gitlab.com/docking/ubuntu-rust-mini/container_registry) for new tag
