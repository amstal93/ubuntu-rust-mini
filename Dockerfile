# ubuntu-rust-mini

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=1.0
ARG UBUNTU_VERSION=18.04
ARG RUST_VERSION=1.51.0


FROM ${DOCKER_REGISTRY_URL}ubuntu:${CUSTOM_VERSION}-${UBUNTU_VERSION} AS ubuntu-rust-mini

ARG RUST_VERSION

WORKDIR /root

ENV RUSTUP_HOME=/usr/local/lib/rustup

RUN \
	curl --output ./rustup-init --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs ; \
	chmod a+x ./rustup-init ; \
	CARGO_HOME=/usr/local \
	./rustup-init -y --no-modify-path --default-toolchain ${RUST_VERSION} --profile minimal ; \
	rm ./rustup-init
